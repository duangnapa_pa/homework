﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Driver;
using MongoDB.Bson;
using LearnMongo.Models;
using MongoDB.Driver.Builders;

namespace LearnMongo.Controllers
{
	public class HomeController : Controller
	{
		//
		// GET: /Home/

		public ActionResult Index()
		{
			return View(new Employee());
		}

		[HttpPost]
		public ActionResult SearchCustomer(string email, string name)
		{
			MongoClient client = new MongoClient();
			MongoServer server = client.GetServer();
			server.Connect();
			MongoDatabase database = server.GetDatabase("Employee");

			var query3 = Query.And(
			   Query.Matches("email", new BsonRegularExpression("/" + email + "/")),
				 Query.Matches("name", new BsonRegularExpression("/" + name + "/")));
			MongoCollection<BsonDocument> employees = database.GetCollection("EmployeeData");
			Employee data = new Employee();
			data.employees = database.GetCollection<EmployeeData>("EmployeeData").Find(query3);

			return View("Index", data);
		}

		[HttpGet]
		public ActionResult CreateCustomer()
		{
			return View();
		}

		[HttpPost]
		public ActionResult CreateCustomer(EmployeeData data)
		{
			MongoClient client = new MongoClient();
			MongoServer server = client.GetServer();
			server.Connect();
			MongoDatabase database = server.GetDatabase("Employee");
			MongoCollection<BsonDocument> employees = database.GetCollection("EmployeeData");
			BsonDocument employee = new BsonDocument{
			{"name",data.name},
			{"email",data.email},
			{"level",data.level},
			{"createddate",DateTime.Now}
			};

			var result = employees.Insert(employee);
			if (result.Ok)
			{
				TempData["MessageSuccess"]= "ดำเนินการเรียบร้อยแล้วค่ะ";
			}

			return View("CreateCustomer",new EmployeeData());
		}

		[HttpPost]
		public ActionResult EditCustomer(string id)
		{
			MongoClient client = new MongoClient();
			MongoServer server = client.GetServer();
			server.Connect();
			MongoDatabase database = server.GetDatabase("Employee");

			var query = Query.EQ("_id", new ObjectId(id));

			MongoCollection<BsonDocument> employees = database.GetCollection("EmployeeData");	
			EmployeeData empdata = database.GetCollection<EmployeeData>("EmployeeData").FindOne(query);

			return View(empdata);
		}

		[HttpPost]
		public ActionResult SaveEditCustomer(EmployeeData data,string id)
		{
			MongoClient client = new MongoClient();
			MongoServer server = client.GetServer();
			server.Connect();
			MongoDatabase database = server.GetDatabase("Employee");

			var query = Query.EQ("_id", new ObjectId(id));

			var update = Update<EmployeeData>.
							Set(p => p.name, data.name).
							Set(p => p.email, data.email).
							Set(p => p.level,data.level);

			var result = database.GetCollection<Employee>("EmployeeData").Update(query, update);

			if (result.Ok)
			{
				TempData["MessageSuccess"] = "ดำเนินการเรียบร้อยแล้วค่ะ";
			}

			return View("EditCustomer", data);
		}


		public ActionResult DeleteCustomer(string id)
		{
			MongoClient client = new MongoClient();
			MongoServer server = client.GetServer();
			server.Connect();
			MongoDatabase database = server.GetDatabase("Employee");
			var query = Query.EQ("_id", new ObjectId(id));

			var result = database.GetCollection<Employee>("EmployeeData").Remove(query);

			if (result.Ok)
			{
				TempData["MessageSuccess"] = "ดำเนินการเรียบร้อยแล้วค่ะ";
			}

			return RedirectToAction("Index");
		}




	}
}
