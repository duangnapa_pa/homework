﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace LearnMongo.Models
{
	public class EmployeeData
	{
		public ObjectId _id { get; set; }
		public string name { get; set; }
		public string email { get; set; }
		public string level { get; set; }
		[BsonElementAttribute("createddate")]
		public DateTime CreateDate { get; set; }
	}
}