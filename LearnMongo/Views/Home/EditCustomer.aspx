﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<LearnMongo.Models.EmployeeData>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>CreateCustomer</title>
</head>
<body>
	<%Html.BeginForm("SaveEditCustomer", "Home", FormMethod.Post); %>
	<div>
		<h3>
			Edit Customer</h3>
		<div>
			<label>
				name:
			</label>
			<%=Html.TextBoxFor(m => m.name) %>
		</div>
		<br />
		<div>
			<label>
				email:
			</label>
			<%=Html.TextBoxFor(m => m.email)%>
		</div>
		<br />
		<div>
			<label>
				level:
			</label>
			<%=Html.TextBoxFor(m => m.level)%>
		</div>
		<br />
		<input type="submit" name="save" value="Save" />
		<%=Html.Hidden("id",Model._id) %>
		<span style="color: Red;">
			<%=TempData["MessageSuccess"]%></span>
	</div>
	<%=Html.ActionLink("Back","Index","Home") %>
	<%Html.EndForm(); %>
</body>
</html>
