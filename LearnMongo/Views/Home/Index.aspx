﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<LearnMongo.Models.Employee>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Index</title>
</head>
<body>
	<div>
		<h3>
			User Information</h3>
		<%Html.BeginForm("CreateCustomer", "Home", FormMethod.Get); %>
		<div>
			<input type="submit" name="CreateUser" value="Create User" />
		</div>
		<%Html.EndForm(); %>
		<%Html.BeginForm("SearchCustomer", "Home", FormMethod.Post); %>
		<div>
			<fieldset>
				<legend>ค้นหาข้อมูล</legend>
				<br />
				<div>
					<label>
						email:
					</label>
					<input type="text" name="email" value="" />
				</div>
				<br />
				<div>
					<label>
						Name:
					</label>
					<input type="text" name="name" value="" />
				</div>
				<br />
				<input type="submit" name="search" value="Search" />
				<br />
			</fieldset>
		</div>
		<%Html.EndForm(); %>
	</div>
	<%if (Model.employees != null)
   {%>
	<div>
		<table>
			<thead>
				<tr>
					<th>
						Name
					</th>
					<th>
						Email
					</th>
					<th>
						Level
					</th>
					<th>
						Createddate
					</th>
					<th colspan="2">
						Action
					</th>
				</tr>
			</thead>
			<%foreach (var item in Model.employees)
	 {%>
			<tbody>
				<tr>
					<td>
						<%=item.name%>
					</td>
					<td>
						<%=item.email%>
					</td>
					<td>
						<%=item.level%>
					</td>
					<td>
						<%=item.CreateDate%>
					</td>
					<td>
						<%Html.BeginForm("EditCustomer", "Home", FormMethod.Post); %>
						<input type="submit" name="edit" value="Edit" />
						<%=Html.Hidden("id",item._id) %>
						<%Html.EndForm(); %>
					</td>
					<td>
						<%Html.BeginForm("DeleteCustomer", "Home", FormMethod.Post); %>
						<input type="submit" name="delete" value="Delete" />
						<%=Html.Hidden("id",item._id) %>
						<%Html.EndForm(); %>
					</td>
				</tr>
			</tbody>
			<%} %>
		</table>
	</div>
	<%} %>
</body>
</html>
